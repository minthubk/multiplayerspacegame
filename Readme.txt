Java overhead view multiplayer space game using LibGDX and KryoNet, for Android and Desktop.

Fly through space, dodge asteroids and shoot your friends.

WASD / 1st Touch Drag - Move
Shift / 1st Touch Drag further - Turbo
Space / 2nd Touch Hold - Shoot
QE / 2nd Touch Drag(TODO) - Zoom

Alternatively, Arrow Keys / RightCtrl work on the Desktop too.


License: Creative Commons Attribution-ShareAlike 3.0 Unported
http://creativecommons.org/licenses/by-sa/3.0/